#!/usr/bin/env python

"""\
Demo: Simple USSD example

Simple demo app that initiates a USSD session, reads the string response and closes the session
(if it wasn't closed by the network)

Note: for this to work, a valid USSD string for your network must be used.
"""

from __future__ import print_function
import re
import logging

PORT = '/dev/ttyUSB0'
BAUDRATE = 9600 
PIN = None # SIM card PIN (if any)

balance = '*141*9*0#'
transfer = '*141*9*3#'

from gsmmodem.modem import GsmModem

def main():
    print('Initializing modem...')
    #logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
    modem = GsmModem(PORT, BAUDRATE)
    modem.connect()
    modem.waitForNetworkCoverage(10)

    print('Sending USSD string: {0}'.format(balance))
    response = modem.sendUssd(balance) # response type: gsmmodem.modem.Ussd
    print('USSD reply received: {0}'.format(response.message))

    if response.sessionActive:
        # At this point, you could also reply to the USSD message by using response.reply()
        response.cancel()
    else:
        print('USSD session was ended by network.')
	mo = re.search( 'You have (\d+) MTN .*', response.message, re.M|re.I)
	if mo:
	  bal = mo.group(0)
	print('>', bal)
	print('Closing USSD session.')
    modem.close()

if __name__ == '__main__':
    main()
